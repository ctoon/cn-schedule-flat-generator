# CN Schedule Flat Generator

This project will read the database and dump schedules in plaintext.

## Output
### Folder structure
Path: `yyyy/mm/dd/source.txt`

Example: `2018/04/15/tvguide.txt` for the April 15th, 2018 schedule extracted from TV Guide

### File structure
First line is a `#` followed by the date in `yyyy-mm-dd` format.  
Then all next following lines are a table of 3 columns: hour (12-hours format), show and title(s).  
Each column is delimited by a space, pipe and space: ` | `

Example:
```plain
# 2018-04-15
 6:00 am | An awesome show | An episode title
 7:00 am | MOVIE | A big movie
 9:30 am | Some show | With one episode; And an another
10:30 am | Or more | Like this Part 1/Like this Part 2
```

## Install
- Clone this repo with `git clone https://gitlab.com/ctoon/cn-schedule-flat-generator.git`
- Go inside the folder `cd cn-schedule-flat-generator`
- Clone the tracking repo `git clone https://gitlab.com/ctoon/cn-schedule-flat.git files`
- copy `config.php.example` to `config.php` and set your DB login
- install deps with `composer install`
- run `php index.php`
- create a cron to run
