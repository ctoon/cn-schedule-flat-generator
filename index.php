<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config.php';

// Function to create the files
function saveToFile($data, $path, $s) {
    if ($data[$s]) {
        $date = explode('-', $data['date']);
        $json = json_decode($data[$s]);
        $content = "# " . $data['date'];

        try {
            foreach ($json as $b) {
                $content .= "\n" . (strlen($b->time) == 7 ? ' ' . $b->time : $b->time);
                $content .= ' | ' . $b->show;
                if ($b->title) $content .= ' | ' . $b->title;
            }

            file_put_contents($path . '/' . $s . '.txt', $content);
        } catch (Exception $e) {
            var_dump($json);
        }
    }
}

// Query DB
$bdd = new PDO('mysql:host=' . $dbHost . ';dbname=' . $dbName . ';charset=utf8', $dbUser, $dbPass);
$req = $bdd->query('SELECT * FROM days;');

// Go through files
while ($data = $req->fetch()) {
    $date = explode('-', $data['date']);
    $path = __DIR__ . '/files/' . $date[0] . '/' . $date[1] . '/' . $date[2];

    // create dir if it doesn't exists
    if (!is_dir($path)) {
        echo "Creating " . $path . "\n";
        mkdir($path, 0777, true);
    }

    // Create/update files if there's something
    saveToFile($data, $path, 'cn');
    saveToFile($data, $path, 'zap');
    saveToFile($data, $path, 'tvguide');
    saveToFile($data, $path, 'as');
    saveToFile($data, $path, 'other');
}

// Close DB
$req->closeCursor();

// Load repo
$repo = new Cz\Git\GitRepository(__DIR__ . '/files');

// Send new data if there's change
if ($repo->hasChanges()) {
    $msg = 'Update @ ' . gmdate('c');
    echo "> " . $msg . "\n";
    $repo->addAllChanges();
    $repo->commit($msg);
    $repo->push('origin');
}
